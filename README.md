# README #

### Hands-On Deep Learning ###

* Curso de extensão Hands-On Deep Learning do Departamento de Ciência da Computação da Universidade Federal de Minas Gerais (DCC/UFMG)
* Para mais informações, acesse: http://deeplearning.dcc.ufmg.br/

### Instrutores ###

* Camila Laranjeira (camila.laranjeira@dcc.ufmg.br)
* Hugo Oliveira (oliveira.hugo@dcc.ufmg.br)
